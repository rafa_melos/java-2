package com.bhlangonijr.service;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageService.class);
    private static List<Message> messages = new ArrayList<>();
    /**
     * Validate and store this message into the database
     *
     * @param message
     * @return
     * @throws Exception
     */
    public Response send(Message message) {
        Response response;

        if(message.getFrom().equals("me@earth"))
        {
            response = new Response(message.getText()+" - has been processed");
            response.setSuccess(true);
        }
        else
        {
            response = new Response();
            response.setSuccess(false);
        }
        // ..... validate message and store

        if (response.getSuccess()) {
            //store the message
            messages.add(message);
        } else {
            //error response
            response.setText("Message cannot be sourced by martians!");
        }
        return response;
    }

    public List<Message> getAllMessages() {
        return messages;
    }

    public Message getById(String id) {
        for(Message m : messages)
        {
            if(m.getId().equals(id))
            {
                return m;
            }
        }
        return null;
    }

}
